#!/bin/sh

set -o errexit

#
OS=$( go env GOOS || echo -n "linux" )
ARCH=$( go env GOARCH || echo -n "amd64" )

BIN_DIR="${BIN_DIR:-"~/bin"}"

if [ ! -e "${BIN_DIR}" ]; then
    mkdir -p "${BIN_DIR}"
fi

KIND_VERSION="v0.12.0"
KUBECTL_VERSION="$(curl --fail --location --silent https://dl.k8s.io/release/stable.txt)"

# https://kind.sigs.k8s.io/docs/user/quick-start/
if [ ! -e "${BIN_DIR}/kind" ]; then
    curl --silent --fail --location --output "${BIN_DIR}/kind" \
        "https://github.com/kubernetes-sigs/kind/releases/download/${KIND_VERSION}/kind-${OS}-${ARCH}"
    chmod +x "${BIN_DIR}/kind"
fi

# https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
if [ ! -e "${BIN_DIR}/kubectl" ]; then
    curl --silent --fail --location --output "${BIN_DIR}/kubectl" \
        "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/${OS}/${ARCH}/kubectl"
    printf "$( curl --silent --fail --location https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/${OS}/${ARCH}/kubectl.sha256 )  ${BIN_DIR}/kubectl\n" | shasum --algo 256 --check 2>/dev/null
    chmod +x "${BIN_DIR}/kubectl"
fi
