What I should have done: GitOps
=================================

Manage a sample app's lifecycle through GitOps..

That sample app:

* has a redis DB.
* has two web components, each serving a single endpoint.
* needs an ingress with appropriate TLS certs
* need the two web components to communicate directly with one another, not via the ingress.

Background
-----------

A job interview for a DevOps role involved a technical task, the GitOps aspect of which went disastrously.
This repo is a follow-up to learn and document ways in which it could have gone better.

Goals
------

* bootstrap a cluster to the point it can be fully managed using GitOps methodologies and tools.
* minimise tool-specific configuration, and minimise duplication.
* support multiple tools, multiple clusters, and multiple environments per cluster.

Requirements
------------

* curl
* docker
* GNU make

Quick start
------------

Run all necessary steps to deploy infrastructure and demo applications for various gitops toolkits/frameworks.

### argocd

```
make argocd
```

### fluxcd

```
make fluxcd
```

Usage
-----

### Install tools

tl;dr `make tools`.

Installs the following, to `~/bin/` by default.

* [kind](https://kind.sigs.k8s.io/)
* [kubectl](https://kubernetes.io/docs/reference/kubectl/kubectl/)

### Deploy a cluster

tl;dr `make cluster`

This creates a local cluster using [`kind`](https://kind.sigs.k8s.io/).
It also creates (or updates) a configuration file for `kubectl`, to [allow interacting with the cluster](https://kind.sigs.k8s.io/docs/user/quick-start/#interacting-with-your-cluster).

### Bootstrap the cluster

tl;dr `make bootstrap`

Deploy generic assets for a given GitOps framework (such as `argo-cd`, `flux`, `fleet`).
Currently, this will deploy `argo-cd` by default.

### Deploy applications to your cluster

tl;dr `make deploy`

Deploy [`argocd`](https://argoproj.github.io/cd), and use it to provision various additional applications to your cluster.
