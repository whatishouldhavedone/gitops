# redis

Deploy a non-persistent, non-authenticated redis instance.

This is suitable only for quick demos and prototyping, and should
not be used for any real-world applications.

A more solid version of this chart would generally be part of an application catalog.

**NOTE** This base _does not_ inherit from the `shared` application base to avoid
conflicts when it is used in the `demo` application present elsewhere in this repository.