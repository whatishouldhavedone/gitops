# bootstrap

This folder contains manifests and assets needed to bootstrap a gitops-managed cluster.

Typically, the applications defined here need to be deployed as is, via `kubectl`.

These applications will typically define their own CRDs for defining things like repositories,
projects, applications, and credentials for accessing repositories and additional clusters.


