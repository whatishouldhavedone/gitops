.PHONY : argocd bootstrap cluster deploy destroy fleet fluxcd tools

BIN_DIR ?= ~/bin
CLUSTER_TYPE ?= argocd
CLUSTER_NAME ?= gitops-$(CLUSTER_TYPE)
WAIT_TIMEOUT ?= 5m

argocd: CLUSTER_TYPE := argocd
argocd: cluster bootstrap
	kubectl wait --namespace argocd --for condition=Ready pod --selector app.kubernetes.io/name=argocd-server  --timeout $(WAIT_TIMEOUT)
	make deploy CLUSTER_TYPE=argocd CLUSTER_NAME=$(CLUSTER_NAME)

fluxcd: CLUSTER_TYPE := fluxcd
fluxcd: cluster bootstrap
	kubectl wait --namespace flux-system --for condition=Ready pod --selector app=source-controller --timeout $(WAIT_TIMEOUT)
	make deploy CLUSTER_TYPE=fluxcd CLUSTER_NAME=$(CLUSTER_NAME)

fleet: CLUSTER_TYPE := fleet
fleet:  cluster bootstrap
	kubectl wait --namespace fleet-system --for condition=Ready pod --selector app=fleet-controller --timeout $(WAIT_TIMEOUT)
	make deploy CLUSTER_TYPE=fleet CLUSTER_NAME=$(CLUSTER_NAME)


bootstrap: tools
	$(BIN_DIR)/kubectl apply -k bootstrap/$(CLUSTER_TYPE)/

deploy: tools
	$(BIN_DIR)/kubectl apply -k clusters/$(CLUSTER_NAME)/infrastructure/
	$(BIN_DIR)/kubectl apply -k clusters/$(CLUSTER_NAME)/demo/

cluster: tools
	@( $(BIN_DIR)/kind get clusters | grep $(CLUSTER_NAME) ) || $(BIN_DIR)/kind create cluster --name $(CLUSTER_NAME) --config config/kind.yaml --wait $(WAIT_TIMEOUT)

destroy: tools
	$(BIN_DIR)/kind delete cluster --name $(CLUSTER_NAME)

tools:
	$(shell BIN_DIR=$(BIN_DIR) scripts/tools.sh)

