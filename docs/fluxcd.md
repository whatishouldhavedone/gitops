# Notes on fluxcd

## install not bootstrap

`fluxcd bootstrap` encourages (somewhat forcefully IMO) deploying both fluxcd
infrastructure and a source repo in a single step.

This is in contrast to `argocd`, that deploys its infrastructure and leaves
the following steps to be decided.

Using `fluxcd install --export`, it is possible to generate the infrastructure manifests
alone (they are also available from `https://github.com/fluxcd/flux2/manifests/install?ref=main`)
While `fluxcd`'s v2 actually defaults to a read-only approach to source repositories,
`bootstrap` seeks to make commits to kick things off.

The 'guidance' this implies isn't unwelcome, but is also not necessary, and is
probably not ideal for a variety of uses cases.

As such, it makes more sense IMO to use `install`, and drive the initial setup
manually.

## group by namespace

`fluxcd` provides no ACL or RBAC support of its own, and instead uses namespaces and
k8s's own RBAC objects.

This does tend to 'encourage' duplication of some assets (git repositories notably),
although kustomize can be used to allow reuse where necessary/appropriate, and
fluxcd does allow pointing to repository objects in 'other' namespaces.
Doing does however risk creating links between 'projects' that in some cases should
not necessarily be interdependent.

Related, decisions (and documentation) about how to manage fluxcd object related to individual
projects are necessary: one possible approach is to define a generic namespace (e.g. `demo` or `demo-app`) holding
fluxcd objects related to a given project, and per-env namespaces (e.g. `demo-dev` or `demo-dev-app`) for individual
instances.
However, doing so can result in a large number of namespaces being define - at scale, this may be undesirable,
and it may be preferrable to group fluxcd objects under per-team namespaces.
It may also be acceptable to have all such objects defined in a single namespace (e.g. `flux-system`).

## healthchecks need to be defined explicitly

## deployments can have RBAC

While the default is for kustomization/helm controllers to have cluster-admin acess,
this can be limited where appropriate.

https://fluxcd.io/docs/components/kustomize/kustomization/#role-based-access-control

## reconcile source often, deployments based on needs.

Source objects in fluxcd benefit from short reconciliation times.
How short depends a little on how many source objects end up pointing to a given server/repository/bucket,
making the reuse across projects of single source objects a good way of limiting load on their backends.
This essentially defines the delay between a change being pushed/merged, and its being applied to the cluster(s).

Reconciliation times on e.g. kustomization objects defines the amount of time the environment can be in a state
different from that defined in git.
In general, this _probably_ should be short, but for cases where it might be necessary to massage certain settings
by hand during dev/testing, it may be permissible for longer durations to be used.
