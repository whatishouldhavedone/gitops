# Repo layout

tl;dr: https://gitlab.com/gitlab-org/gitlab/-/issues/329773 seems to suggest a
reasonable approach for a kustomize-driven gitops infra that would work with most tools.

```
apps
├── shared # some shared config, used by all apps (e.g. labels, annotations)
│   ├── kustomization.yaml
│   └── config.yaml
├── app1
|   ├── base
|   │   ├── deployment.yaml
|   │   ├── kustomization.yaml # refers to "../../shared"
|   │   └── service.yaml
|   └── overlays
|       ├── dev
|       │   ├── kustomization.yaml
|       │   └── patch.yaml
|       ├── prod
|       │   ├── kustomization.yaml
|       │   └── patch.yaml
|       └── staging
|           ├── kustomization.yaml
|           └── patch.yaml
├── app2
    ├── base
    │   ├── deployment.yaml
    │   ├── kustomization.yaml # refers to "../../shared"
    │   └── service.yaml
    └── overlays
        ├── dev
        |   └── ...
        ├── prod
        |   └── ...
        └── staging
            └── ...
```

You may want to consider having overlays in an `environments` directory rather than co-located with apps, but
this risks making the repository more unwieldy for users (more spread means more looking for things in not-necessarily-obvious places),
as well as for some tools (such as `fleet`, currently at least).

## Terminology

/!\ **WARNING** What follows is heavily simplistic and focused on the 'needs' of this document - check out some of the references
for more complete definitions.

Frequent use of terms defined by `kustomize` is made throughout this document.
It may help to keep https://kubectl.docs.kubernetes.io/references/kustomize/glossary/ open.

An application in this document can generally refer interchangeably to either a single container image or
to a multi-component project (e.g. gitlab).
Note that GitOps methodology can be applied to things that aren't container images, although this is not
something this document discusses.

### Code repository

A code repository holds code and other assets from which an application (or components of an application)
can be built, and packaged.

For example, a `go` codebase, some scripts for building it (or similar, e.g. `Makefile`, `build.gradle`, `package.json`, etc),
and packaging it (typically, this isn't actually a script but a Dockerfile).
There are typically assets for testing the codebase involved.

Typically, a continuous integration and continuous delivery (a.k.a `CI/CD`) pipeline is defined within this repository, to automate
building, testing, and packaging steps, as well as distribution of build and/or packaging artifacts.

A code repository can produce one or more deployable artifacts.
Within the context of this document, deployable artifacts essentially mean a container image.

### Configuration repository

In contrast to a code repository, the configuration repository contains the assets necessary to deploy zero or more applications.

In the context of this document, this means manifests for deploying an application's components and dependencies (e.g. datastores)
to kubernetes.

### Configuration catalog

A configuration catalog is a [configuration repository](#configuration-repository) that describes multiple applications,
typically in a way that allows (and/or encourages) reusing its contents across multiple other applications.

Good candidates for a configuration catalog are things like datastores, metric collection systems, service meshes - or
more generally, relatively generic applications that tend to be reused across multiple projects or applications.

Configuration catalog applications _generally_ don't have overlays - they typically provide only a base that
can be customised by overlays from 'client' applications.

### Source of truth

In the context of GitOps, this typically means a configuration repository containing a description of the
desired state of one or more deployments of a given application in a version control system, typically `git`.
This source of truth can be composed of references to other source of truth as well as of locally-stored
configuration assets.

[Configuration repository](#configuration-repository) and [configuration catalogs](#configuration-catalogs) are
both sources of truth, the former often referencing the latter.

Typically, multiple deployments of an application exist to reflect different environments (e.g. `dev`, `qa`, `prod`).
In some cases, there are also multiple branches involved in the version control system.
However, relying on multiple branches is not typically encouraged in the context of GitOps, and so this document
assumes all environments are defined on a single branch. More on this later.

### GitOps

GitOps is a continuous deployment methodology that focuses on [configuration repository](#configuration-repository)
as a source of truth that define the state of zero or more applications in a declarative format.

In the context of this document, the focus is on using `kubernetes` manifests and/or `helm` charts, processed
through `kustomize`.

GitOps focuses on a [reconciliation loop](#reconciliation-loop) to ensure running deployments are in the desired state.

### Reconciliation loop

For a given application (or (application, environment) pair) deployment, a GitOps toolkit monitors the state of
deployment on `kubernetes`, and regularly checks it against the state defined by the deployment's source of truth.
When a deviation is found, a GitOps toolkit or framework (e.g. `argocd`) reconciles the state of the running
deployment to match the source of truth.

Deviations will typically occur for two reasons:
* a change is pushed to the source of truth (e.g. a pull/merge request is merged)
* a change is made to the running deployment (e.g. an engineer changes a configuration file to test something)

## General principles

### Isolate application deployment from application code.

It is currently considered best practice to maintain deployment assets separate from code assets.

There are two basic reasons for this:

* it isn't always possible or practical to maintain all application code in a single repository.
  Even if it may initially, the application may well evolve in a direction that makes doing so difficult.
* it isn't always desirable for a change in code to trigger reconciliation, which would typically happen
  when code changes are merged.

### Application and catalog repositories

[Configuration catalogs](#configuration-catalogs) are a good way of maintaining centralised definitions for
widely used components like datastores.
It is _definitely_ not necessary to maintain one, but it is often useful to do so, notably as a way of
encouraging best practices in application deployments, and to make fixing issues (including security issues)
more easily.

The layout described at the start of this document can apply to both.
Typically, [configuration catalogs](#configuration-catalogs) would not have environment-specific overlays,
but _may_ have an overlay to provide highly available deployments, for example.

### Use kustomize

This document focuses on using static manifests and [`kustomize`](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/).

This is a preference, but by no means intended to discourage using `helm`.
However, even when using `helm`, it is a good idea to pipe its rendered templates through kustomize to benefit from shared
standards defined in kustomize [bases](https://kubectl.docs.kubernetes.io/references/kustomize/glossary/#base).

How to do so will depend on the GitOps toolkit used.

### One branch, many environments

When maintaining multiple environments (or variants) for a given application, it is generally easier and
more legible to have your source of truth for *all* of them be a single branch.

There isn't any fundamental reason why having a branch per environment couldn't be done, but doing so
can make things more complex for both users and tooling.

### Refer to other applications by URL

Your kustomization files should ideally reference their dependencies by URLs rather than by path.

The simplest for most GitOps toolkits is to use HTTPS to reference bases in public repositories.
Private repositories can be supported by using git over SSH, or using tokens (including basic auth).

It is a good idea to ensure these URLs include a branch ref (typically, `?rev=<BRANCH NAME>`), such
as `https://gitlab.com/whatishouldhavedone/gitops/apps/redis?rev=main` or `git@gitlab.com/whatishouldhavedone/gitops/apps/redis?rev=main`.

A given application's overlays reference their base by path - that base should reference other bases
by URL.

### Limit use of toolkit-specific assets

Insofar as possible, limit the use of assets that rely on CRDs provided by a single GitOps toolkit in your
`kustomize` bases.
`helm` charts are a slightly different matter, since toolkit-specific charts can be used, but the general
recommendation still applies.

To use GitOps toolkits, you will need to define toolkit-specific manifests _somewhere_ - do so in cluster-specifc
directories, and keep your application definitions free of them.
More on how to work with GitOps-driven clusters is discussed below.

### Avoid defining namespaces in bases

This isn't necessarily a GitOps-specific guideline, but is worth mentionning - you should _generally_ define
namespaces for applications in overlays, not in bases.

### READMEs are helpful

Although absent from the layout above, consider including READMEs in your application directories, irrespective of their being within
an application or configuration catalog repository.

Their basic use it to help newcomers orientate themselves, and to document any peculiarities or decisions made when defining an
application base or overlay, in addition to what version control commit messages might provide.

## Cluster management

**WIP** More so than the rest of this document, this section is a work in progress.
Meaning it is almost certainly wrong and misleading in (large) part(s).

Managing a kubernetes cluster using GitOps requires some adapations and changes to the layout and guidelines described so far.

tl;dr: the following layout is one solution, providing a 'root' dir per cluster,

```
bootstrap/
├── argocd
│   ├── kustomization.yaml
│   └── namespace.yaml
├── fleet
│   ├── crds.yaml
│   ├── install.yaml
│   ├── kustomization.yaml
│   └── namespace.yaml
├── fluxcd
│   ├── gotk-components.yaml
│   └── kustomization.yaml
clusters/
├── gitops-argocd
│   ├── demo
│   │   ├── demo-applicationset.yaml
│   │   ├── demo-project.yaml
│   │   └── kustomization.yaml
│   └── infrastructure
│       ├── infrastructure-applicationset.yaml
│       ├── infrastructure-application.yaml
│       ├── infrastructure-project.yaml
│       └── kustomization.yaml
├── gitops-fleet
│   ├── demo
│   │   ├── demo-repository.yaml
│   │   └── kustomization.yaml
│   └── infrastructure
│       ├── infrastructure-repository.yaml
│       └── kustomization.yaml
└── gitops-fluxcd
    ├── demo
    │   ├── demo-dev-kustomization.yaml
    │   ├── demo-prod-kustomization.yaml
    │   ├── demo-qa-kustomization.yaml
    │   ├── kustomization.yaml
    │   └── namespace.yaml
    └── infrastructure
        ├── infrastructure-kustomization.yaml
        ├── infrastructure-repository.yaml
        ├── kustomization.yaml
        └── namespace.yaml
```

The structure suggested above is basically directory per cluster, then directory per application.
In this case, the application can be a number of related applications - the infrastructure 'application'
would generally deploy things like an ingress controller, policy toolkit, monitoring, and so on.
Similarly, the `demo` application defined at cluster level would deploy several instances, often one
per overlay defined for the application (where such overlays generally map to environments).

Such cluster applications would typically map to a given project or team, and should aim to make


### Guidelines

#### Some manual steps are unavoidable

Manual here means 'not driven by git events and reconciliation'.

Some toolkits can manage mutliple clusters, but deploying the toolkit itself,
i.e. bootstrapping your GitOps environment, involves at least one manual step.

You can automate that step (script it, use an infrastructure-as-code solution like `terraform`, etc),
the point being that this initial step is not itself going to be handled by the GitOps toolkit you choose,
for obvious reasons.

_Updates_ to the toolkit can be dealt with using the toolkit itself, if you so choose.
Some toolkits make this easier than others, so don't feel obligated to do so - do what seems best,
and offers the best compromise between stability and feature availability.

Deploying further applications can (and possibly should) also be done manually, rather than
by using reconciliation of cluster configuration directories.
Some toolkits lean more strongly than others in this direction, and doing so ensures that new
applications can be added at any point after their definitions are ready, rather than as soon
as they are ready.

Here too: 'manual' can be scripted and/or automated, and means doing so outside of the chosen
toolkit's reconciliation loop.

#### Avoid framework/toolkit-specific tools

Both `argocd` and `fluxcd` encourage the use of specific, product-specific binaries to deal with
bootstrapping a cluster.
There's no fundamental issue listening to that advice, but it (IMO) runs counter to the intent of GitOps,
and requires involving extra tools in managing clusters.

Instead, consider relying only on `kubectl apply -k` to bootstrap your cluster(s).
It makes things more visible, easier to look up when needed, and provides both rollback and auditing.
In other words, many of the benefits of adopting GitOps methodologies.

#### Avoid customising apps in cluster directories

You can absolutely refer to one cluster's application directories(/bases) from another cluster's,
but should avoid applying customisations to the applications being deployed at cluster level.

Instead, create a suitable overlay for the application itself, and reference it in the cluster-level manifests.

In other words, the presence of `kustomization.yaml` in the layout described above is about re-use, not
actual customisation of applications.

#### Here be toolkit-specific objects

Other than the `kustomization.yaml` for each cluster's applications, and a possible `namespace.yaml`,
manifests defined for each cluster's applications will use CRDs defined by the GitOps toolkit used for
that cluster.

If you are going to manage upgrades to that toolkit using the toolkit itself, make sure to create a directory
for this.
However tempting, avoid mixing it in with the `infrastructure` directory.
