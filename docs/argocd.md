# Notes on argocd

## Project of projects isn't a pattern

Applying app of apps to manage multiple projects does not work.
This is due to it ending up with all applications from 'subprojects' being
owned by the project managing/declaring others.

It may be possible to use applicationSets to work around this, doing
so remains to be tested.

Projects generally _seem_ to be silos.

## App of apps vs applicationSet

App of apps makes for a more compact top-level view.

However, in conjunction with some project level filtering, applicationSet provides
more visibility, at the possible cost of making the UI rather busy when a lot of applications
are defined.

## Using generic methods rather than argocd specific ones costs

Related to the above, attempting to avoid (too much) use of argocd-specific
objects - specifically Application objects - results in a less-than-ideal _UI_
experience.

Deploying `infrastructure` assets using a single kustomization file passed to
argocd via an Application results in a single, huge application that groups all
components under a single root.

App of apps would be cleanest, and an applicationset would provide a clearer overview,
but both require a lot of argocd-specific manifests that aren't necessary to actually
deploy things, and that aren't reusable by other systems (e.g. `fluxcd`).

It isn't a showstopper (or blocker) by any stretch, but does make for a slightly lumpy experience
that doesn't necessarily make the most of what argocd has to offer.
This is really more of a gripe than anything, but does mean that certain constraints make
argocd's UI feature a lot less useful than it could be.
